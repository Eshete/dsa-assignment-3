
import ballerina/io;
import ballerinax/kafka;

const string SASL_URL = "localhost:9093";


kafka:ProducerConfiguration producerConfig = {

       mechanism: kafka:AUTH_SASL_PLAIN,

      sername: "Kandjimi",
      password: "Eshete@987"
    },
    securityProtocol: kafka:PROTOCOL_SASL_PLAINTEXT
};



kafka:Producer kafkaProducer = check new(SASL_URL, producerConfig);

public function main() returns error? {
    string message = "Hello, World!";
    check kafkaProducer->send({
        topic: "demo-security",
        value: message.toBytes()
    });
    check kafkaProducer->'flush();
    io:println("Message published successfully.");
}

